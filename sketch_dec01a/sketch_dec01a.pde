int ledPin = 13;
//Ονομάζουμε το pin 13 "ledPin"
int speakerPin = 12;
//Ονομάζουμε το pin 12 "speakerPin"
 
#define c 261
#define d 294
#define e 329
#define f 349
#define g 391
#define gS 415
#define a 440
#define aS 455
#define b 466
#define cH 523
#define cSH 554
#define dH 587
#define dSH 622
#define eH 659
#define fH 698
#define fSH 740
#define gH 784
#define gSH 830
#define aH 880
//Οι συχνότητες των νοτών που θα χρησιμοποιήσουμε

void setup()    
{        
  pinMode(ledPin,OUTPUT);
  //Το pin με όνομα "ledPin" θα είναι έξοδος
  pinMode(speakerPin, OUTPUT);  
  //Το pin με όνομα "speakerPin" θα είναι έξοδος
}        
         
void loop()     // Κύριο loop
{
  march_theme();
}        
         
void beep(unsigned char speakerPin, int frequency, long time)
{
    digitalWrite(ledPin, HIGH);  
    //Ρυθμικό led!
   
    int i;      
    long delay_time = (long)(1000000/frequency);
    long loop_time = (long)((time*1000)/(delay_time*2));
    for (i=0; i<loop_time; i++)    
    {    
        digitalWrite(speakerPin,HIGH);
        delayMicroseconds(delay_time);
        digitalWrite(speakerPin,LOW);
        delayMicroseconds(delay_time);
    }    
   
    digitalWrite(ledPin, LOW);
   
    delay(20);
}        
         
void march_theme()
{        
    //500 ms για νότα ενός τετάρτου
   
    beep(speakerPin, a, 500);
    beep(speakerPin, a, 500);    
    beep(speakerPin, a, 500);
    beep(speakerPin, f, 350);
    beep(speakerPin, cH, 150);
   
    beep(speakerPin, a, 500);
    beep(speakerPin, f, 350);
    beep(speakerPin, cH, 150);
    beep(speakerPin, a, 1000);
   
    beep(speakerPin, eH, 500);
    beep(speakerPin, eH, 500);
    beep(speakerPin, eH, 500);    
    beep(speakerPin, fH, 350);
    beep(speakerPin, cH, 150);
   
    beep(speakerPin, gS, 500);
    beep(speakerPin, f, 350);
    beep(speakerPin, cH, 150);
    beep(speakerPin, a, 1000);
   
    beep(speakerPin, aH, 500);
    beep(speakerPin, a, 350);
    beep(speakerPin, a, 150);
    beep(speakerPin, aH, 500);
    beep(speakerPin, gSH, 250);
    beep(speakerPin, gH, 250);
   
    beep(speakerPin, fSH, 125);
    beep(speakerPin, fH, 125);    
    beep(speakerPin, fSH, 250);
    delay(250);
    
    beep(speakerPin, aS, 250);    
    beep(speakerPin, dSH, 500);  
    beep(speakerPin, dH, 250);  
    beep(speakerPin, cSH, 250);  
   
    beep(speakerPin, cH, 125);  
    beep(speakerPin, b, 125);  
    beep(speakerPin, cH, 250);      
    delay(250);
    beep(speakerPin, f, 125);  
    beep(speakerPin, gS, 500);  
    beep(speakerPin, f, 375);  
    beep(speakerPin, a, 125);
   
    beep(speakerPin, cH, 500);
    beep(speakerPin, a, 375);  
    beep(speakerPin, cH, 125);
    beep(speakerPin, eH, 1000);

   
    beep(speakerPin, aH, 500);
    beep(speakerPin, a, 350);
    beep(speakerPin, a, 150);
    beep(speakerPin, aH, 500);
    beep(speakerPin, gSH, 250);
    beep(speakerPin, gH, 250);
   
    beep(speakerPin, fSH, 125);
    beep(speakerPin, fH, 125);    
    beep(speakerPin, fSH, 250);
    delay(250);
    beep(speakerPin, aS, 250);    
    beep(speakerPin, dSH, 500);  
    beep(speakerPin, dH, 250);  
    beep(speakerPin, cSH, 250);  

    beep(speakerPin, cH, 125);  
    beep(speakerPin, b, 125);  
    beep(speakerPin, cH, 250);      
    delay(250);
    beep(speakerPin, f, 250);  
    beep(speakerPin, gS, 500);  
    beep(speakerPin, f, 375);  
    beep(speakerPin, cH, 125);
           
    beep(speakerPin, a, 500);            
    beep(speakerPin, f, 375);            
    beep(speakerPin, c, 125);            
    beep(speakerPin, a, 1000);      
}
 
